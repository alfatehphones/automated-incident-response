# Automated Incident Response

Welcome to the open-source repository containing automation scripts developed by the GitLab Security Incident Response team. These scripts are designed to streamline and optimize the incident response management process, reduce administrative overhead, and enhance overall efficiency during incidents. By sharing these scripts, we aim to contribute to the global incident response community and empower teams to build their own Security Orchestration, Automation, and Response (SOAR) solutions tailored to their specific requirements.

## Table of Contents

- [Introduction](#introduction)
- [Features](#features)
- [Getting Started](#getting-started)
  - [Prerequisites](#prerequisites)
  - [Installation](#installation)
- [Usage](#usage)
  - TBA
- [Contributing](#contributing)
- [License](#license)

## Introduction

Incident response involves various processes beyond initial investigations, including alert management, communication, incident tracking, handoffs, and more. These processes can often be tedious, repetitive, and time-consuming, particularly in remote work environments. The GitLab Incident Response team has developed a collection of automation scripts that primarily operate within the Slack ecosystem while integrating with critical cloud-based systems, such as GitLab, PagerDuty, SIEM, and Google Workspace. By leveraging these scripts, we have significantly optimized our incident response workflow and reduced investigation time.

## Features

Our automation scripts cover the following key functionalities:

- **Alert Deployment**: Automate the deployment of alerts through our detection-as-code CI/CD pipeline.
- **Detections in Slack**: Forward SIEM alerts to Slack with an interactive module.
- **Team Handoffs**: Facilitate seamless handoffs between different incident response team members.

## Getting Started

### Prerequisites

Before using the automation scripts, ensure you have the following in place:

- Slack workspace with appropriate permissions.
- Accounts/API access to GitLab, PagerDuty, SIEM system, and Google Workspace (if applicable).
- Access to the Tines automation platform.

### Installation

The scripts apply to multiple platforms. Clone this repository to your local environment to access them:

```bash
git clone https://gitlab.com/gitlab-com/gl-security/security-operations/gitlab-sirt-public/automated-incident-response.git
cd automated-incident-response
```

## Usage

Below, you'll find a brief overview of each included script and its usage.

### TBA

### TBA

### TBA

## Contributing

Contributions from the community to improve and expand these automation scripts will be accepted in the near future. 

## License

This project is licensed under the [MIT License](LICENSE).

---

We hope that these automation scripts prove valuable in enhancing your incident response processes. Feel free to adapt, modify, and integrate them into your workflow as needed. We look forward to your contributions and feedback!